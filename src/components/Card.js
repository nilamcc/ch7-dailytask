import React from "react";
import Button from "./Button";

const Card = (props) => {
  const { title, description, imgSrc, imgAlt, btnText, btnHref } = props;

  return (
    <div className="card" style={{ width: "18rem" }}>
      <img src={imgSrc} className="card-img-top" alt={imgAlt} />
      <div className="card-body">
        <h5 className="card-title">{title}</h5>
        <p className="card-text">{description}</p>
        <a href={btnHref}><Button variant="somewhere" className="btn btn-primary">{btnText}</Button></a>
      </div>
    </div>
  )
}

export default Card;
