import React from 'react';
import classNames from "classnames"
import { PropTypes } from "prop-types"
import style from "./Button.module.css"

const backgroundColor = {
  primary: "#89CFF0",
  secondary: "#E5CEE8",
  black: "#BF5859",
  somewhere :"#9DC183"
}

const textColor = {
  primary: "#fdfff5",
  secondary: "#fdfff5",
  black: "#fdfff5",
  somewhere :"#fdfff5"
}

const Button = ({ children, variant, onClick, className, ...props }) => {
  const styles = {
    backgroundColor: backgroundColor[variant] || backgroundColor.primary,
    color: textColor[variant] || textColor.primary,
  };

  return (
    <button style={styles} {...props} onClick={onClick} className={classNames(style.Button, className)}>
      {children}
    </button>
  )
}

Button.propTypes = {
  variant: PropTypes.oneOf([
    "primary",
    "secondary",
    "black"
  ]),
  onClick: PropTypes.func,
}

Button.defaultProps = {
  variant: "primary",
  onClick: () => {},
}

export default Button;
