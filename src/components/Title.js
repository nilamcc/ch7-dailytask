import React from "react";
import classNames from "classnames"
import style from "./Title.module.css"

const Title = (props) => {
  const { title } = props;

  return (
    <div className={classNames(style.Title, title)}>
      <div className="card-body">
        <h1 className="card-title">{title}</h1>
      </div>
    </div>
  )
}

export default Title;